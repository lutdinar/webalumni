<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_forumBisnis extends CI_Model
{

    function getAllForumBisnis()
    {
        $this->db->join('tb_jenis_bisnis', 'tb_forum_bisnis.jenis_bisnis_id = tb_jenis_bisnis.id_jenis_bisnis');
        $this->db->join('tb_anggota', 'tb_forum_bisnis.anggota_id = tb_anggota.id_anggota');
        $this->db->order_by('nama_forum_bisnis', 'ASC');
        return $this->db->get('tb_forum_bisnis')->result();
    }

    function findForumBisnis($where)
    {
        $this->db->where($where);
        $this->db->join('tb_jenis_bisnis', 'tb_forum_bisnis.jenis_bisnis_id = tb_jenis_bisnis.id_jenis_bisnis');
        $this->db->join('tb_anggota', 'tb_forum_bisnis.anggota_id = tb_anggota.id_anggota');
        $this->db->order_by('nama_forum_bisnis', 'ASC');

        return $this->db->get('tb_forum_bisnis')->result();
    }

    function insertForumBisnis($forumBisnis)
    {
        $this->db->insert('tb_forum_bisnis', $forumBisnis);
    }

    function updateForumBisnis($forumBisnis, $id)
    {
        $this->db->where('id_forbis', $id);
        $this->db->update('tb_forum_bisnis', $forumBisnis);
    }

    function deleteForumBisnis($id)
    {
        $this->db->where('id_forbis', $id);
        $this->db->delete('tb_forum_bisnis');
    }

}

