<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="javascript:void(0);">Anggota</a></li>
    <li><a href="<?= base_url('koordinator/Anggota/kelolaAnggota'); ?>">Kelola Anggota</a></li>
    <li class="active"><a href="<?= base_url('koordinator/Anggota/detailAnggota/'.$anggota[0]->id_anggota); ?>">Detail Anggota</a></li>
</ul>

<div class="page-title">                 
   <h2> Detail Anggota</h2>
</div>

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-3">

            <div class="panel panel-default">
                <div class="panel-body profile" style="background: url('<?= base_url("assets/back-end/img/darkblurbg.jpg"); ?>') center center no-repeat;">
                    <div class="profile-image">
                        <img src="<?= base_url('uploads/avatars/'.$anggota[0]->nama_foto); ?>" alt="<?= $anggota[0]->nama_lengkap; ?>"/>
                    </div>
                    <div class="profile-data">
                        <div class="profile-data-name"><?= $anggota[0]->nama_lengkap; ?></div>
                        
                        <?php if ($anggota[0]->status_bekerja != NULL || $anggota[0]->status_bekerja != "") { ?>

							<?php if ($anggota[0]->status_bekerja == 0 && $anggota[0]->bisnis_usaha == 0) { ?>
								<div class="profile-data-title" style="color: #fff;">Tidak Bekerja</div>
							<?php } else if ($anggota[0]->status_bekerja == 0 && $anggota[0]->bisnis_usaha == 1) { ?>
								<div class="profile-data-title" style="color: #fff;">Tidak Bekerja / Punya Usaha</div>
							<?php } else { ?>
								<div class="profile-data-title" style="color: #fff;"><?= $anggota[0]->jabatan; ?></div>
							<?php } ?>

						<?php } else { ?>
							<div class="profile-data-title" style="color: #fff;">Profesi belum diisi</div>
						<?php } ?>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="contact-info">
                        <?php if (!empty($anggota[0]->email) || $anggota[0]->email != NULL) { ?>
                            <p><small><?= $anggota[0]->email; ?></small></p>
                        <?php } else { ?>
                            <p><small>Email belum diisi</small></p>
                        <?php } ?>
                        
                        <?php if (!empty($anggota[0]->angkatan) || $anggota[0]->angkatan != NULL) { ?>
                            <p><small><?= $anggota[0]->angkatan; ?></small></p>
                        <?php } else { ?>
                            <p><small>Angkatan belum diisi</small></p>
                        <?php } ?>
                        
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-9 pull-right">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Data Diri</h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Lengkap :</label>  
                            <div class="col-md-9">
                                <label class="control-label"><?= $anggota[0]->nama_lengkap; ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Jenis Kelamin :</label>
                            <div class="col-md-3">
                                <?php if ($anggota[0]->jenis_kelamin == 1) { ?>
                                    <label class="control-label">Laki-Laki</label>
                                <?php } else if ($anggota[0]->jenis_kelamin == 2) { ?>
                                    <label class="control-label">Perempuan</label>
                                <?php } else { ?>
                                    <label class="control-label">Belum diisi</label>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Panggilan / alias :</label>
                            <div class="col-md-9">
                                <label class="control-label"><?= $anggota[0]->nama_panggilan_alias;; ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Tempat, Tanggal Lahir:</label>
                            <div class="col-md-6">
                                <label class="control-label"><?= $anggota[0]->tempat_lahir; ?>, </label>
                                <label class="control-label"><?= $anggota[0]->tanggal_lahir; ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Angkatan :</label>
                            <div class="col-md-9">
                                <label class="control-label"><?= $anggota[0]->angkatan; ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Golongan Darah :</label>
                            <div class="col-md-9">
                                <label class="control-label"><?= $anggota[0]->golongan_darah; ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Telepon / Telp. Alternatif :</label>
                            <div class="col-md-2">
                                <label class="control-label"><?= $anggota[0]->no_telp; ?></label>
                            </div>
                            <div class="col-md-2">
                                <label class="control-label"><?= $anggota[0]->no_telp_alternatif; ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">NIK :</label>
                            <div class="col-md-9">
                                <label class="control-label"><?= $anggota[0]->NIK; ?></label>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Email :</label>
                            <div class="col-md-9">
                                <label class="control-label"><?= $anggota[0]->email; ?></label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-9 pull-right">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Domisili</h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Negara :</label>  
                            <div class="col-md-9">
                                <label class="control-label"><?= $anggota[0]->negara; ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Provinsi :</label>                                        
                            <div class="col-md-9">
                                <label class="control-label"><?= $anggota[0]->provinsi; ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Kabupaten / Kota :</label>                                       
                            <div class="col-md-9">                            
                                <label class="control-label"><?= $anggota[0]->kabupaten_kota; ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Alamat Lengkap :</label>
                            <div class="col-md-9">
                                <label class="control-label"><?= $anggota[0]->alamat; ?></label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-9 pull-right">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Profesi</h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Pendidikan Terakhir :</label>  
                            <div class="col-md-9">
                                <label class="control-label"><?= $anggota[0]->pendidikan_terakhir; ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Status Bekerja :</label>
                            <div class="col-md-9">
                                <label class="control-label"><?= $anggota[0]->status_bekerja; ?></label>
                            </div>
                        </div>                    

                        <div class="form-group">
                            <label class="col-md-3 control-label">Bidang Industri :</label>
                            <div class="col-md-9">
                                <label class="control-label"><?= $anggota[0]->bidang_industri; ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Jabatan :</label>          
                            <div class="col-md-9">
                                <label class="control-label"><?= $anggota[0]->jabatan; ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Perusahaan :</label>
                            <div class="col-md-9">
                                <label class="control-label"><?= $anggota[0]->nama_perusahaan; ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Bisnis / Usaha :</label>     
                            <div class="col-md-9">
                                <label class="control-label"><?= $anggota[0]->bisnis_usaha; ?></label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-9 pull-right">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Info Program</h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-5">
                                <label class="check"><input type="checkbox" value="checked" id="infoProgram1" class="icheckbox" disabled /> Sosial Pendidikan</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-5">
                                <label class="check"><input type="checkbox" value="checked" id="infoProgram2" class="icheckbox" disabled /> Sosial Kemanusiaan</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-5">
                                <label class="check"><input type="checkbox" value="checked" id="infoProgram3" class="icheckbox" disabled /> Pengembangan Sarana Prasarana</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-5">
                                <label class="check"><input type="checkbox" value="checked" id="infoProgram4" class="icheckbox" disabled /> Silaturahim Kebersamaan</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-5">
                                <label class="check"><input type="checkbox" value="checked" id="infoProgram5" class="icheckbox" disabled /> Penawaran Sponsorship &amp; Donasi</label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-9 pull-right">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Keanggotaan</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-5">
                            <label class="check"><input type="checkbox" class="icheckbox" value="checked" id="keanggotaan1" disabled /> Support</label>
                        </div>
                        <div class="col-md-9">
                            <p>Iuran Pendaftaran sebesar Rp. 10.000 (sepuluh ribu rupiah) 1x seumur hidup.</p>
                            <p>Iuran Wajib Tahunan sebesar Rp. 25.000 (dua puluh lima ribu rupiah).</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-5">
                            <label class="check"><input type="checkbox" class="icheckbox" value="checked" id="keanggotaan2" disabled /> Loyalist</label>
                        </div>
                        <div class="col-md-9">
                            <p>Iuran sukarela sebesar : </p>
                            <?php if ($info[0]->iuran_sukarela == NULL || $info[0]->iuran_sukarela == 0) { ?>
                                <label class="control-label">Rp. 0</label>
                            <?php } else { ?>
                                <label class="control-label"><?= $info[0]->iuran_sukarela; ?></label>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- END PAGE CONTENT WRAPPER --> 

<script type="text/javascript">

	$("#file-simple").fileinput({
		showUpload: false,
		showCaption: false,
		browseClass: "btn btn-danger",
		fileType: "any"
	});

</script>