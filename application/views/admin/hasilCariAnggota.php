<div class="row">
            <?php foreach ($anggota as $A) { ?>
            <div class="col-md-3">
                <!-- CONTACT ITEM -->
                <div class="panel panel-default">
                    <div class="panel-body profile">
                        <div class="profile-image">
                            <?php if ($A->nama_foto == NULL) { ?>
                                <img src="<?php echo base_url('uploads/no-image.jpg'); ?> " alt="No Image" title="Default Image">
                            <?php } else { ?>
                                <img src="<?php echo base_url('uploads/avatars/'.$A->nama_foto); ?> " alt="<?= $A->nama_lengkap; ?>" title="<?= $A->nama_lengkap; ?>">
                            <?php } ?>
                        </div>
                        <div class="profile-data">
                            <div class="profile-data-name"><?= $A->nama_lengkap; ?></div>

                            <?php if ($A->status_bekerja != NULL || $A->status_bekerja != "") { ?>

                                <?php if ($A->status_bekerja == 0 && $A->bisnis_usaha == 0) { ?>
                                    <div class="profile-data-title">Tidak Bekerja</div>
                                <?php } else if ($A->status_bekerja == 0 && $A->bisnis_usaha == 1) { ?>
                                    <div class="profile-data-title">Tidak Bekerja / Punya Usaha</div>
                                <?php } else { ?>
                                    <div class="profile-data-title"><?= $A->jabatan; ?></div>
                                <?php } ?>

                            <?php } else { ?>
                                <div class="profile-data-title">Profesi belum diisi</div>
                            <?php } ?>
                            
                        </div>
                        <div class="profile-controls">
                            <a class="profile-control-left btn-ubah-anggota" title="Ubah" id="<?= $A->id_anggota; ?>" data-toggle="modal" data-target="#ubahAnggota"><span class="fa fa-edit"></span></a>
                            <a class="profile-control-right btn-hapus-anggota" title="Hapus" id="<?= $A->id_anggota; ?>" data-toggle="modal" data-target="#message-box-delete-anggota"><span class="fa fa-trash-o"></span></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="contact-info">
                            <?php if ($A->no_telp == "") { ?>
                                <p><small>Mobile</small><br>Belum di isi</p>
                            <?php } else { ?>
                                <p><small>Mobile</small><br><?= $A->no_telp; ?></p>
                            <?php } ?>

                            <?php if ($A->email == "") { ?>
                                <p><small>Email</small><br>Belum di isi</p>
                            <?php } else { ?>
                                <p><small>Email</small><br><?= $A->email; ?></p>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="panel-footer text-center">
                        <a class="btn btn-info btn-rounded btn-block" href="<?= base_url('admin/Anggota/detailAnggota/'.$A->id_anggota); ?>" title="Lihat">Lihat</a>
                    </div>
                </div>
                <!-- END CONTACT ITEM -->
            </div>
            <?php } ?>
        </div>