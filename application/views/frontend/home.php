    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1 id="homeHeading">Selamat Datang Di IKASMA3BDG</h1>
                <hr>
                <p>Temukan informasi mengenai alumni di sini, temukan teman anda di sini dan mengobrol dengan topik yang ada</p>
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Jelajahi</a>
            </div>
        </div>
    </header>

    <section class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Apa Itu IKASMA3BDG ?</h2>
                    <hr class="light">
                    <p class="text-faded">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <a href="#services" class="page-scroll btn btn-default btn-xl sr-button">Ada Apa Saja ?</a>
                </div>
            </div>
        </div>
    </section>

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">IKASMA3BDG</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-diamond text-primary sr-icons"></i>
                        <h3>Informasi Alumni</h3>
                        <p class="text-muted">Temukan informasi alumni yang up to date disini</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 text-center">
                    <div class="service-box">
						<a href="<?= base_url('Home/cariTeman'); ?>"><i class="fa fa-4x fa-paper-plane text-primary sr-icons"></i></a>
                        <h3>Teman</h3>
						<a href="<?= base_url('Home/cariTeman'); ?>" style="text-decoration: none;"><p class="text-muted">Temukan teman sekolah anda disini</p></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 text-center">
                    <div class="service-box">
                        <a href="<?= base_url('Home/forumBisnisAnggota'); ?>"><i class="fa fa-4x fa-newspaper-o text-primary sr-icons"></i></a>
                        <h3>FORBIS</h3>
                        <a href="<?= base_url('Home/forumBisnisAnggota'); ?>" style="text-decoration: none;"><p class="text-muted">Forum Bisnis</p></a>
                    </div>
                </div>    
            </div>
        </div>
    </section>

    <section class="no-padding" id="portfolio">
        <div class="container-fluid">
            <div class="row no-gutter popup-gallery">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <a href="<?php echo base_url('assets/back-end/img/1.jpeg') ?>" class="portfolio-box">
                        <img src="<?php echo base_url('assets/back-end/img/1.jpeg') ?>" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Category
                                </div>
                                <div class="project-name">
                                    Project Name
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <a href="<?php echo base_url('assets/back-end/img/2.jpeg') ?>" class="portfolio-box">
                        <img src="<?php echo base_url('assets/back-end/img/2.jpeg') ?>" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Category
                                </div>
                                <div class="project-name">
                                    Project Name
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <a href="<?php echo base_url('assets/back-end/img/12.jpeg') ?>" class="portfolio-box">
                        <img src="<?php echo base_url('assets/back-end/img/12.jpeg') ?>" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Category
                                </div>
                                <div class="project-name">
                                    Project Name
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <a href="<?php echo base_url('assets/back-end/img/13.jpeg') ?>" class="portfolio-box">
                        <img src="<?php echo base_url('assets/back-end/img/13.jpeg') ?>" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Category
                                </div>
                                <div class="project-name">
                                    Project Name
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <a href="<?php echo base_url('assets/back-end/img/1.jpeg') ?>" class="portfolio-box">
                        <img src="<?php echo base_url('assets/back-end/img/1.jpeg') ?>" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Category
                                </div>
                                <div class="project-name">
                                    Project Name
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <a href="<?php echo base_url('assets/back-end/img/15.jpeg'); ?>" class="portfolio-box">
                        <img src="<?php echo base_url('assets/back-end/img/15.jpeg'); ?>" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Category
                                </div>
                                <div class="project-name">
                                    Project Name
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <aside class="bg-dark">
        <div class="container text-center">
            <div class="call-to-action">
                <h2>Temukan teman anda disini</h2>
                <a href="<?= base_url('Home/cariTeman'); ?>" class="btn btn-default btn-xl sr-button">Cari sekarang</a>
            </div>
        </div>
    </aside>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Kontak</h2>
                    <hr class="primary">
                    <p>Hubungi kami di</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x sr-contact"></i>
                    <p>08122311547</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p><a href="mailto:anggota.ikasma3bdg@gmail.com">anggota.ikasma3bdg@gmail.com</a></p>
                </div>
            </div>
        </div>
    </section>
