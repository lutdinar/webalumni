<!DOCTYPE html>
<html>
<head>
    <title><?= $title; ?></title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
        
    <link rel="icon" href="<?= base_url('assets/html/favicon.ico'); ?>" type="image/x-icon" />
    <!-- END META SECTION -->
        
    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('assets/html/css/theme-default.css') ?>"/>
    <!-- EOF CSS INCLUDE -->

    <!-- START PLUGINS -->
    <script type="text/javascript" src="<?php echo base_url('assets/html/js/plugins/jquery/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/html/js/plugins/jquery/jquery-ui.min.js') ?>"></script>
    <script type='text/javascript' src="<?php echo base_url('assets/html/js/plugins/jquery-validation/jquery.validate.js') ?>"></script>
    <!-- END PLUGINS -->

</head>
<body>

    <div class="page-title" style="margin-top: 10px;">
        <h2> Cari Teman Anda</h2>
    </div>

	<!-- START BREADCRUMB -->
	<ul class="breadcrumb">
		<li><a href="<?= base_url('Home'); ?>">Beranda</a></li>
		<li class="active"><a href="<?= base_url('Home/cariTeman'); ?>">Cari Teman Anda</a></li>
	</ul>
	<!-- END BREADCRUMB -->

    <div class="page-content-wrap">

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<p>Cari teman</p>
						<div class="form-group">
							<div class="col-md-12">
								<div class="input-group">
									<div class="input-group-addon">
										<span class="fa fa-search"></span>
									</div>
									<input type="text" class="form-control" id="keyword" name="namaAnggota" placeholder="Siapa yang akan anda cari ?">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="hasilCari">
			<div class="row">
				<?php foreach ($dataAnggota as $A) { ?>
					<div class="col-md-3">
						<!-- CONTACT ITEM -->
						<div class="panel panel-default">
							<div class="panel-body profile">
								<div class="profile-image">
									<?php if ($A->nama_foto == NULL || empty($A->nama_foto)) { ?>
										<img src="<?php echo base_url('uploads/no-image.jpg'); ?> " alt="Default Image" title="Default Image">
									<?php } else { ?>
										<img src="<?php echo base_url('uploads/avatars/'.$A->nama_foto); ?> " alt="<?= $A->nama_lengkap; ?>" title="<?= $A->nama_lengkap; ?>">
									<?php } ?>
								</div>
								<div class="profile-data">
									<?php if ($A->support == 1) { ?>
										<div class="profile-data-name"><?= $A->nama_lengkap; ?> <span class="badge badge-success">Member</span></div>
									<?php } else { ?>
										<div class="profile-data-name"><?= $A->nama_lengkap; ?></div>
									<?php } ?>

									<?php if ($A->angkatan == NULL || empty($A->angkatan)) { ?>
										<div class="profile-data-title">Belum di isi</div>
									<?php } else { ?>
										<div class="profile-data-title"><?= $A->angkatan; ?></div>
									<?php } ?>
								</div>
							</div>
							<div class="panel-body">
								<div class="contact-info">
									<?php if ($A->email == "") { ?>
										<p><small>Email :</small> Belum di isi</p>
									<?php } else { ?>
										<p><small>Email :</small> <?= $A->email; ?></p>
									<?php } ?>
								</div>
							</div>
						</div>
						<!-- END CONTACT ITEM -->
					</div>
				<?php } ?>
			</div>
		</div>

    </div>

    <!-- START PRELOADS -->
    <audio id="audio-alert" src="<?php echo base_url('assets/html/audio/alert.mp3') ?>" preload="auto"></audio>
    <audio id="audio-fail" src="<?php echo base_url('assets/html/audio/fail.mp3') ?>" preload="auto"></audio>
    <!-- END PRELOADS -->

    <!-- START SCRIPTS -->
    <script type="text/javascript" src="<?php echo base_url('assets/html/js/plugins/bootstrap/bootstrap.min.js') ?>"></script>

    <!-- START THIS PAGE PLUGINS-->
    <script type="text/javascript" src="<?php echo base_url('assets/html/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/html/js/plugins/scrolltotop/scrolltopcontrol.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/html/js/plugins/owl/owl.carousel.min.js') ?>"></script>                 

    <!-- TAB  PLUGINS -->
    <script type="text/javascript" src="<?php echo base_url('assets/html/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') ?>"></script>
    <!-- TAB PLUGINS -->    

    <!--TABLE PLUGINS -->
    <script type="text/javascript" src="<?php echo base_url('assets/html/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/html/js/plugins/datatables/jquery.dataTables.min.js') ?>"></script>    
    <!--TABLE PLUGINS -->  

    <!-- START TEMPLATE -->
    <script type="text/javascript" src="<?php echo base_url('assets/html/js/plugins.js') ?>"></script>        
    <script type="text/javascript" src="<?php echo base_url('assets/html/js/actions.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/html/js/demo_dashboard.js') ?>"></script>
    <!-- END TEMPLATE -->

    <script type="text/javascript" src="<?php echo base_url('assets/html/js/custom-javascript.js'); ?>"></script>
	
	<script>
		$('#keyword').on('keyup', function () {
			$('#hasilCari').load('<?= base_url("Home/cariAnggota?keyword=") ?>'+$('#keyword').val());
		});
	</script>
    <!-- END SCRIPTS --> 

</body>
</html>