<div class="row">
	<?php foreach ($dataAnggota as $A) { ?>
	<div class="col-md-3">
		<!-- CONTACT ITEM -->
		<div class="panel panel-default">
			<div class="panel-body profile">
				<div class="profile-image">
					<?php if ($A->nama_foto == NULL || empty($A->nama_foto)) { ?>
						<img src="<?php echo base_url('uploads/no-image.jpg'); ?> " alt="Default Image" title="Default Image">
					<?php } else { ?>
						<img src="<?php echo base_url('uploads/avatars/'.$A->nama_foto); ?> " alt="<?= $A->nama_lengkap; ?>" title="<?= $A->nama_lengkap; ?>">
					<?php } ?>
				</div>
				<div class="profile-data">
					<?php if ($A->support == 1) { ?>
						<div class="profile-data-name"><?= $A->nama_lengkap; ?> <span class="badge badge-success">Member</span></div>
					<?php } else { ?>
						<div class="profile-data-name"><?= $A->nama_lengkap; ?></div>
					<?php } ?>

					<?php if ($A->angkatan == NULL || empty($A->angkatan)) { ?>
						<div class="profile-data-title">Belum di isi</div>
					<?php } else { ?>
						<div class="profile-data-title"><?= $A->angkatan; ?></div>
					<?php } ?>
				</div>
			</div>
			<div class="panel-body">
				<div class="contact-info">
					<?php if ($A->email == "") { ?>
						<p><small>Email :</small> Belum di isi</p>
					<?php } else { ?>
						<p><small>Email :</small> <?= $A->email; ?></p>
					<?php } ?>
				</div>
			</div>
		</div>
		<!-- END CONTACT ITEM -->
	</div>
	<?php } ?>
</div>