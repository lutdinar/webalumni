<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
 * class Home Koordinator
 * Created by Lut Dinar Fadila 2018
*/

class Home extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_anggota');

        if ($this->session->userdata('logged_in') == '' && $this->session->userdata('username') == '' && $this->session->userdata('role') == '') {
            redirect('login');
        } elseif ($this->session->userdata('logged_in') == 'Sudah Login' && $this->session->userdata('role') == '1') {
            redirect('admin');
        } elseif ($this->session->userdata('logged_in') == 'Sudah Login' && $this->session->userdata('role') == '3') {
            redirect('anggota');
        }
        
    }

    public function index()
    {
        $data['title'] = 'Beranda Koordinator';
        $data['info'] = $this->M_anggota->findAnggota('*', array('tb_anggota.user_id = ' => $this->session->userdata('uid')));
        $this->koordinator_render('koordinator/home', $data);
    }

}
