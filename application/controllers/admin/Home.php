<?php

if (defined('BASEPATH') or exit('No direct script access allowed'));

/*
 * class Home Admin
 * Created by Lut Dinar Fadila 2018
*/

class Home extends MY_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('M_anggota');
        $this->load->model('M_forumBisnis');

        if ($this->session->userdata('logged_in') == '' && $this->session->userdata('username') == '' && $this->session->userdata('role') == '') {
            redirect('login');
        } elseif ($this->session->userdata('logged_in') == 'Sudah Login' && $this->session->userdata('role') == 2) {
            redirect('koordinator');
        } elseif ($this->session->userdata('logged_in') == 'Sudah Login' && $this->session->userdata('role') == 3) {
            redirect('anggota');
        }
    }

    function index() {
        $data['title'] = 'Beranda Admin';
        $data['info'] = $this->M_anggota->findAnggota('*', array('tb_anggota.user_id' => $this->session->userdata('uid')));
        
        $totalCalonAnggota = $this->M_anggota->findAnggota('*', array('tb_anggota.status_anggota' => "0"));
        $data['calonAnggota'] = count($totalCalonAnggota);

        $where = array(
            'status_anggota' => "1",
            'nama_lengkap != ' => "admin"
        );
        $totalAnggota = $this->M_anggota->findAnggota('*', $where);
        $data['anggotaAktif'] = count($totalAnggota);

        // $forumBisnisAnggota = $this->M_forumBisnis->getAllForumBisnis();
        
        // if (!empty($forumBisnisAnggota)) {
        //     echo count($forumBisnisAnggota);
        // } else {
        //     echo "Forbis kosong";
        // }
        
        
        $this->admin_render('admin/home', $data);
    }
}