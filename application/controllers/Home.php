<?php

if (defined('BASEPATH') or exit('No direct script access allowed'));

/*
 * class Home Front Page
 * Created by Lut Dinar Fadila 2018
*/

class Home extends MY_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('M_forumBisnis');
        $this->load->model('M_anggota');
    }
    
    function index() {
        $data['title'] = 'Beranda IKASMA3BDG';
        $this->frontend_render('frontend/home', $data);
    }
    
    function cariTeman() {
        $data['title'] = 'Cari Teman';
        $data['dataAnggota'] = $this->M_anggota->findAnggota('*', array('tb_anggota.nama_lengkap != ' => 'admin'));

        $this->load->view('frontend/cariTeman', $data);
    }

    function cariAnggota()
    {
        $keyword = $_GET['keyword'];

        $where = array(
            'tb_anggota.nama_lengkap != ' => 'admin'
        );
        $data['dataAnggota'] = $this->M_anggota->findAnggotaLikeNama($where, $keyword);

        $this->load->view('frontend/hasilCari', $data);
    }

    function forumBisnisAnggota() {
        $data['title'] = 'Forum Bisnis IKASMA3BDG';
        $data['forbis'] = $this->M_forumBisnis->getAllForumBisnis();

        $this->load->view('frontend/forumBisnis', $data);
    }
    
}
